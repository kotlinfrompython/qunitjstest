pluginManagement {
    resolutionStrategy {
        eachPlugin {

            if (requested.id.toString() == "kotlin2js") {
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:${requested.version}")
            }
        }
    }
}
rootProject.name = "myapp"

//include(":myapp")
//include(":src/main/kotlin/com.example.myapp/myapp")


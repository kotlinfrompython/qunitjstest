import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.kotlin.dsl.get
import java.io.File

class CopyJsFiles : Plugin<Project> {

    override fun apply(project: Project) = project.run {
        //        tasks {
//            register("copyJsFiles") {
//                val compileClasspath = configurations["compileClasspath"]
//                compileClasspath.forEach { jarFile ->
//                    extractFromJarFile(jarFile, file("$buildDir/classes/main/lib"))
//                }
//            }
//        }
    }
}

fun Project.copyJsFiles() {
    findAndCopyJarFiles(configurations["compileClasspath"])
    findAndCopyJarFiles(configurations["testCompileClasspath"])
}

fun Project.findAndCopyJarFiles(compileClasspath: Configuration) {
    compileClasspath.apply {
        forEach { jarFile ->
            extractFromJarFile(jarFile, file("$buildDir/classes/test/lib"))
        }
    }
}

fun Project.extractFromJarFile(jarFile: File, outputDir: File) {
    copy {
        includeEmptyDirs = false
        from(zipTree(jarFile))
        into(outputDir)
        include("**/*.js")
        exclude("META-INF/**", "**/*.meta.js")
    }
}
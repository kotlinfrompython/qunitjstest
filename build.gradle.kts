import org.gradle.kotlin.dsl.execution.ProgramText.Companion.from
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

buildscript {
    repositories {
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
        jcenter()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Share.kotlin_version}")
    }
}

plugins {
    id("kotlin2js") version Share.kotlin_version
    //`CopyJsFiles`
}
//apply from: "kotlinjs.gradle" -- not sure this is correct way to do this right now
//rootProject.apply { from(rootProject.file("kotlinjs.gradle")) }

repositories {
    mavenCentral()
    maven("https://plugins.gradle.org/m2/")
    jcenter()
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-stdlib-js:${Share.kotlin_version}")
    testCompile("org.jetbrains.kotlin:kotlin-test-js:${Share.kotlin_version}")
    //classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.2.61")
    compile(kotlin("stdlib-js"))
}

tasks {
    //TODO Want to get this into buildSrc... but can't figure how
    "compileKotlin2Js"(Kotlin2JsCompile::class) {
        kotlinOptions {
            //These allow in browser kotlin debugging
            sourceMap = true
            sourceMapEmbedSources = "always"
        }
    }
    copyJsFiles()
}
